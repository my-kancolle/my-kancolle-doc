# MyKanColleDoc References

#### 供参考的可用链接
1. 是谁呼叫舰队（DiabloHu开发）: https://github.com/TeamFleet/WhoCallsTheFleet/
2. 二期（HTML5）api_start2解析：https://bbs.nga.cn/read.php?tid=14868912
3. PSV版（舰队Collection改）相关讨论：https://bbs.nga.cn/read.php?tid=9003959
4. 七四式电子观测仪apilist：https://github.com/andanteyk/ElectronicObserver/blob/develop/ElectronicObserver/Other/Information/apilist.txt

#### 关于初始装备/各项参数的成长公式

1. 如果想要省力的话，可以安装七四式电子观测仪，登录游戏后在工具（Tool）下拉菜单下可选择舰船图鉴/装备图鉴，并导出csv文件（每一栏初始装备对应装备图鉴中的id，0表示为空，-1表示不可用）。上述文件包含舰娘和装备的各项参数（个人认为如果单机化，可能会出现一些需要讨论的Issue）。
2. 无近代化改修的情况下，可认为舰娘的各项指标（改造后，为最低改造等级下的初始值）到99级的值之间为线性增长关系（即ylv=ymod+(lv-mod)*{(y99-ymod)/(99-mod)}，y99为99级下的最终值，ymod为改造后的初始值，lv为当前等级，ylv为当前等级下的值。上述指标多数可在 http://wikiwiki.jp/kancolle 的舰船最大值和简易最终值页面查询得到），回避/对潜/索敌三项参数在100级之后仍然遵循同样的线性增长关系，


#### 目前存在的Issues
1. 关于装备图鉴，是否有必要在数据库中增加一栏注明装备的所属国？
2. 目前页游版的黑箱系统过于繁杂（尤其是适重炮的命中加成或惩罚），个人认为单机版可以将这一系统和目前明确标出的装备buff/debuff合并。
3. 其他不便提出的Issues以NGA站内私信方式发送。

